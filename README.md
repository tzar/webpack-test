# README #

An example from-scratch configuration of webpack showing off some of the different loaders, transpilation and tree-shaking. [Based off this article](https://css-tricks.com/introduction-webpack-entry-output-loaders-plugins/) with some small updates and differences.

To run the dev server:

    npm start

To build:


    npm run build