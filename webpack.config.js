var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HTMLWebpackPlugin = require('html-webpack-plugin');
var ENV = process.env.NODE_ENV;
var webpack = require('webpack');

var sassRules = {
  use: [
    { loader: 'css-loader' },
    { loader: 'sass-loader' },
  ]
};

var baseConfig = {
  entry: {
    main: "./src/index.js"
  },
  output: {
    filename: "[name].js",
    path:     path.resolve("./build"),
    publicPath: "/"
  },
  module: {
    rules: [
      {
        test: /src\/.*\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
        ]
      },
      {
        test: /src\/.*\.sass$/,
        use: ExtractTextPlugin.extract(sassRules)
      },
      {
        test: /src\/.*\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                ['env', {
                  targets: {
                    browsers: ["last 2 versions", "safari >= 7"]
                  },
                  modules: false
                }]
              ]
            }
          }
        ]
      },
      {
        test: /src\/.*\.coffee$/,
        use: [
          { loader: 'coffee-loader' }
        ]
      },
      {
        test: /src\/.*\.jpg$/,
        use: [
          {
            loader: 'url-loader',
            query: { limit: 1000000}
          }
        ]
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('main.css'),
    new HTMLWebpackPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(ENV)
    })
  ]
};

if(ENV === 'production') {
  baseConfig.plugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = baseConfig;
